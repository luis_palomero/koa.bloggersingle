angular.controller('bloggerSingleCtrl', loadListFunction);

loadListFunction.$inject = [ '$scope', '$http', 'structureService',
    '$location', '$compile', 'bloggerPostService' ];

function loadListFunction($scope, $http, structureService, $location, $compile,
    bloggerService) {

  // Register upper level modules
  structureService.registerModule($location, $scope, "bloggerSingle");
  
  var attrs = $scope.bloggerSingle.modulescope;  
  var postId =  $location.search().post || attrs.postId;
  bloggerService.getPost(attrs.blogId, postId, attrs.key).then(
      function onGetBlogSuccess(result) {
        $scope.post = result;
      }, function onGetBlogFailure(err) {
        $scope.error = {
            message: "Error retrieving data",
            err: err
        }
      });
}
