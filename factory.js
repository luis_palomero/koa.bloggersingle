angular
    .module('aa', [])
    .factory(
        'bloggerPostService',
        function($http, $q) {
          var urls = {
            post : 'https://www.googleapis.com/blogger/v3/blogs/:blogId/posts/:postId?key=:apiKey'
          }
          var getPost = function(blogId, postId, key) {
            var deferred = $q.defer();
            var url = urls.post.replace(':blogId', blogId).replace(':postId',
                postId).replace(':apiKey', key);
            $http.get(url).success(function(data) {
              deferred.resolve(data);
            }).error(function(err) {
              deferred.reject(err);
            });

            return deferred.promise;
          }

          var service = {
            getPost : getPost
          }

          return service;
        });
